package main

import (
	"poc-helloasso/internal/config"
	"poc-helloasso/internal/rbmq"
	"poc-helloasso/web"

	"github.com/gin-gonic/gin"

	// CA bundle for docker image
	_ "golang.org/x/crypto/x509roots/fallback"
)

func main() {
	e := gin.Default()
	c := config.GetConfig()

	// Start RabbitMQ connection
	go rbmq.Startup()

	web.Setup(e)

	e.Run(":" + c.Port)
}
