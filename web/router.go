package web

import (
	"encoding/json"
	"fmt"
	"poc-helloasso/internal/cagnottes"
	"poc-helloasso/internal/config"
	"poc-helloasso/internal/rbmq"
	"poc-helloasso/internal/webhook"
	"time"

	"github.com/gin-gonic/gin"
)

func OptionalString(s string) *string {
	if s == "" {
		return nil
	}
	return &s
}

type PaymentData struct {
	Order struct {
		FormSlug         string `json:"formSlug"`
		FormType         string `json:"formType"`
		OrganizationSlug string `json:"organizationSlug"`
	} `json:"order"`
	Amount int32 `json:"amount"`
	Payer  struct {
		Email     string `json:"email"`
		FirstName string `json:"firstName"`
		LastName  string `json:"lastName"`
	} `json:"payer"`
}

func doDon(c *gin.Context, amount string, streamer string, team string, payment *PaymentData, sok, tok bool) {
	donateur := cagnottes.Donateur{
		FirstName: payment.Payer.FirstName,
		LastName:  payment.Payer.LastName,
		Email:     payment.Payer.Email,
		Amount:    int64(payment.Amount),
		Team:      team,
		Streamer:  streamer,
		Time:      time.Now().Unix(),
	}

	donateur.GenID()

	// Add the donateur to the list
	cagnottes.D.AddToDonateur(donateur)

	rbmq.SendDonationTeamInfo(team, donateur)
	rbmq.SendTeamTopDonateurInfo(team)
	rbmq.SendGlobalTopDonateurInfo()
	rbmq.SendLastDonateursInfo()

	cagnottes.D.AddTeam(team, int64(payment.Amount))
	rbmq.SendTeamPotInfo(team, cagnottes.D.GetTeam(team))

	// Add the amount to the total & streamer & team
	cagnottes.D.AddTotal(int64(payment.Amount))

	if sok {
		cagnottes.D.AddStreamer(streamer, int64(payment.Amount))
		// rbmq.SendStreamerPotInfo(streamer, cagnottes.D.GetStreamer(streamer))
	}

	// Send total
	rbmq.SendTotalPotInfo(cagnottes.D.GetTotal())

	// Do this last, we don't really care about this
	discordMessage := fmt.Sprintf("Don reçu : %s€", amount)
	if sok {
		discordMessage = fmt.Sprintf("Don reçu: %s€ (sur la cagnotte de **%s**)", amount, streamer)
	} else if tok {
		discordMessage = fmt.Sprintf("Don reçu: %s€ (sur la cagnotte de **%s**)", amount, team)
	}
	webhook.SendMessageToDiscord(discordMessage)

}

func Setup(g *gin.Engine) {
	type Notif struct {
		Data      interface{}            `json:"data"`
		EventType string                 `json:"eventType"`
		Metadata  map[string]interface{} `json:"metadata"`
	}

	cfg := config.GetConfig()

	g.POST("/", func(c *gin.Context) {
		var notif Notif

		// Print the body
		if cfg.LogLevel == "debug" {
			bdy, _ := c.GetRawData()
			fmt.Println(string(bdy))
			//FIXME: PRINTING THE BODY PREVENTS IT FROM BEING PARSED AGAIN, THIS REQUEST WILL FAIL WITH 404
			//See https://github.com/gin-gonic/gin/issues/439")
		}

		if err := c.ShouldBindJSON(&notif); err != nil {
			c.JSON(400, gin.H{
				"error": err.Error(),
				"msg": "Could not bind notif", 
			})
			return
		}

		if notif.EventType != "Payment" {
			c.JSON(200, gin.H{
				"msg": "Ignored event", 
			})
			return
		}

		data, err := json.Marshal(notif.Data)
		if err != nil {
			c.JSON(500, gin.H{
				"error": err.Error(),
				"msg": "Could not parse data",
			})
			return
		}
		var payment PaymentData
		if err := json.Unmarshal(data, &payment); err != nil {
			c.JSON(500, gin.H{
				"error": err.Error(),
				"msg": "Could not parse payment",
			})
			return
		}

		if payment.Order.OrganizationSlug == "humani-tn" && payment.Order.FormSlug != cfg.HelloAssoConfig.ShopSlug && payment.Order.FormSlug != cfg.HelloAssoConfig.TombolaSlug && payment.Order.FormType != "Checkout" {
			fmt.Println("Ignoring payment due to slug " + payment.Order.FormSlug)
			return
		}

		if payment.Order.FormSlug == cfg.HelloAssoConfig.TombolaSlug {
			cagnottes.D.AddTombola(int64(payment.Amount))
			rbmq.SendTombolaInfo(cagnottes.D.GetTombola())
		}

		if _, ok := notif.Metadata["anonymous"]; ok {
			payment.Payer.FirstName = "Un"
			payment.Payer.LastName = "Anonyme"
		}

		amount := fmt.Sprintf("%d", payment.Amount)
		amount = amount[:len(amount)-2] + "." + amount[len(amount)-2:]
		

		// Force all donations to go to team 0 with no streamer
		doDon(c, amount, "-1", "0", &payment, false, false)
			c.JSON(200, gin.H{
				"success": true,
			})

		/*
		streamerI, sok := notif.Metadata["streamer"]
		teamI, tok := notif.Metadata["team"]

		streamer := fmt.Sprintf("%v", streamerI)
		team := fmt.Sprintf("%v", teamI)

		shareAllTeams := false

		if sok {
			team = cagnottes.D.StreamersToTeams[streamer]
		}

		if tok {
			if team != "0" && team != "1" && team != "2" && team != "3" {
				shareAllTeams = true
			}
		}

		if !sok && !tok {
			// Pick a random team (0-3)
			shareAllTeams = true
		}

		if !shareAllTeams {
			doDon(c, amount, streamer, team, &payment, sok, tok)
			c.JSON(200, gin.H{
				"success": true,
			})
		} else {
			r := rand.Intn(4)
			mod := payment.Amount % 4
			total := payment.Amount / 4

			for i := 0; i < 4; i++ {
				if i == r {
					payment.Amount = total + mod
				} else {
					payment.Amount = total
				}
				doDon(c, amount, "", fmt.Sprintf("%d", i), &payment, false, true)
			}
			c.JSON(200, gin.H{
				"success": true,
			})
		}
		*/
	})

	g.GET("/sendall", func(c *gin.Context) {
		rbmq.SendTotalPotInfo(cagnottes.D.GetTotal())
		rbmq.SendTombolaInfo(cagnottes.D.GetTombola())
		rbmq.SendGlobalTopDonateurInfo()
		rbmq.SendLastDonateursInfo()

		for streamer, pot := range cagnottes.D.Streamers {
			rbmq.SendStreamerPotInfo(streamer, pot)
		}

		for team, pot := range cagnottes.D.Teams {
			rbmq.SendTeamPotInfo(team, pot)
			rbmq.SendTeamTopDonateurInfo(team)
		}

		c.JSON(200, gin.H{
			"success": true,
		})
	})

	g.GET("/reset", func(c *gin.Context) {
		cagnottes.D.Reset()

		rbmq.SendTotalPotInfo(cagnottes.D.GetTotal())
		rbmq.SendTombolaInfo(cagnottes.D.GetTombola())
		rbmq.SendGlobalTopDonateurInfo()
		rbmq.SendLastDonateursInfo()

		for streamer, pot := range cagnottes.D.Streamers {
			rbmq.SendStreamerPotInfo(streamer, pot)
		}

		for i := 0; i < 4; i++ {
			team := fmt.Sprintf("%d", i)
			pot := cagnottes.D.Teams[team]
			rbmq.SendTeamPotInfo(team, pot)
			rbmq.SendTeamTopDonateurInfo(team)
		}

		c.JSON(200, gin.H{
			"success": true,
		})
	})
}
