package api

import (
	"poc-helloasso/helloasso"
	"poc-helloasso/internal/config"
	"poc-helloasso/tnevent"

	"github.com/sirupsen/logrus"
)

var HaClient *helloasso.Client
var TnClient *tnevent.ClientWithResponses

func init() {
	c := config.GetConfig()

	haclient, err := helloasso.NewClient(c.HelloAssoConfig.URL + "/v5")
	if err != nil {
		logrus.Fatal(err)
	}

	_, err = haclient.GetToken()
	if err != nil {
		logrus.Fatal(err)
	}

	tnclient, err := tnevent.NewClientWithResponses(c.TnEventConfig.URL)
	if err != nil {
		logrus.Fatal(err)
	}

	TnClient = tnclient
	HaClient = haclient
	logrus.Info("API clients initialized")
}
