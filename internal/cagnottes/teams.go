package cagnottes

import (
	"context"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"os"
	"poc-helloasso/internal/api"
	"poc-helloasso/tnevent"
	"sort"
	"sync"
	"time"
)

type Donateur struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	ID        string `json:"id"`
	Email     string `json:"-"`
	Amount    int64  `json:"amount"`
	Team      string `json:"-"`
	Streamer  string `json:"-"`
	Time      int64  `json:"time"`
}

func (d *Donateur) GenID() {
	// SHA256 of email
	sha256 := sha256.Sum256([]byte(d.Email))
	d.ID = fmt.Sprintf("%x", sha256)
}

type Data struct {
	Teams     map[string]int64 `json:"teams"`
	Streamers map[string]int64 `json:"streamers"`

	StreamersToTeams map[string]string `json:"streamers_to_teams"`

	Total int64 `json:"total"`

	Tombola int64 `json:"tombola"`

	LastDonateurs []Donateur                     `json:"-"`
	Donateurs     map[string]map[string]Donateur `json:"donateurs"`

	// Tops
	// TeamTops  map[string][]Donateur `json:"-"`
	// GlobalTop []Donateur            `json:"-"`

	sync.Mutex
}

func (d *Data) Reset() {
	d.Lock()
	defer d.Unlock()

	for k := range d.Teams {
		d.Teams[k] = 0
	}
	for k := range d.Streamers {
		d.Streamers[k] = 0
	}

	d.Total = 0
	d.Tombola = 0

	D.LastDonateurs = make([]Donateur, 0)

	for k := range D.Donateurs {
		D.Donateurs[k] = make(map[string]Donateur)
	}

	d.Save()
}

func (d *Data) Save() error {
	b, err := json.Marshal(d)
	if err != nil {
		return err
	}

	return os.WriteFile("data.json", b, 0644)
}

var D *Data

func init() {
	// Load from file data.json
	D = &Data{
		Teams:            make(map[string]int64),
		Streamers:        make(map[string]int64),
		StreamersToTeams: make(map[string]string),
		LastDonateurs:    make([]Donateur, 0),
		Donateurs:        make(map[string]map[string]Donateur),
	}

	s, err := os.ReadFile("data.json")
	if err != nil {
		// Fill with default values
		firstFill()
		D.Save()
		return
	}

	err = json.Unmarshal(s, D)
	if err != nil {
		// Panic because we can't do anything without the data
		panic(err)
	}

	// Fill tops
	// D.TeamTops = make(map[string][]Donateur)
	// D.GlobalTop = make([]Donateur, 0)
	// for team, donateurs := range D.Donateurs {
	// 	var tops []Donateur
	// 	for _, donateur := range donateurs {
	// 		tops = append(tops, donateur)
	// 	}
	// 	sort.SliceStable(tops, func(i, j int) bool {
	// 		return tops[i].Amount > tops[j].Amount
	// 	})

	// 	if len(tops) > 10 {
	// 		tops = tops[:10]
	// 	}

	// 	if team == "global" {
	// 		D.GlobalTop = tops
	// 		continue
	// 	}

	// 	D.TeamTops[team] = tops
	// }

	// Fill last donateurs
	merged := make(map[string]Donateur)
	for _, donateurs := range D.Donateurs {
		for _, donateur := range donateurs {
			v, ok := merged[donateur.ID]
			if !ok {
				merged[donateur.ID] = donateur
				continue
			}
			v.Amount += donateur.Amount
		}
	}

	D.LastDonateurs = make([]Donateur, 0)
	for _, donateur := range merged {
		D.LastDonateurs = append(D.LastDonateurs, donateur)
	}

	sort.SliceStable(D.LastDonateurs, func(i, j int) bool {
		return D.LastDonateurs[i].Time > D.LastDonateurs[j].Time
	})
	if len(D.LastDonateurs) > 10 {
		D.LastDonateurs = D.LastDonateurs[:10]
	}

	// Add missing streamers and teams every 10 minutes
	go func() {
		for {
			addMissing()
			time.Sleep(10 * time.Minute)
		}
	}()
}

func firstFill() {
	resp, err := api.TnClient.GetStreamersStreamersGetWithResponse(context.Background())
	if err != nil {
		panic(err)
	}

	var streamers []tnevent.Streamer
	err = json.Unmarshal(resp.Body, &streamers)
	if err != nil {
		panic(err)
	}

	for _, streamer := range streamers {
		D.Streamers[fmt.Sprint(streamer.Id)] = 0
		teamId := fmt.Sprint(streamer.TeamId)
		D.StreamersToTeams[fmt.Sprint(streamer.Id)] = teamId
		D.Teams[teamId] = 0
	}

	for t := range D.Teams {
		D.Donateurs[t] = make(map[string]Donateur)
	}
	D.Donateurs["global"] = make(map[string]Donateur)
}

func addMissing() {
	resp, err := api.TnClient.GetStreamersStreamersGetWithResponse(context.Background())
	if err != nil {
		panic(err)
	}

	var streamers []tnevent.Streamer
	err = json.Unmarshal(resp.Body, &streamers)
	if err != nil {
		panic(err)
	}

	for _, streamer := range streamers {
		if _, ok := D.Streamers[fmt.Sprint(streamer.Id)]; ok {
			// Check if the team is the same
			teamId := fmt.Sprint(streamer.TeamId)
			if D.StreamersToTeams[fmt.Sprint(streamer.Id)] == teamId {
				continue
			}
			D.StreamersToTeams[fmt.Sprint(streamer.Id)] = teamId
			continue
		}
		D.Streamers[fmt.Sprint(streamer.Id)] = 0
		teamId := fmt.Sprint(streamer.TeamId)
		D.StreamersToTeams[fmt.Sprint(streamer.Id)] = teamId

		if _, ok := D.Teams[teamId]; ok {
			continue
		}
		D.Teams[teamId] = 0
	}

	// Save to file data.json
	err = D.Save()
	if err != nil {
		return
	}
}

func (d *Data) AddTeam(team string, amount int64) {
	d.Lock()
	defer d.Unlock()

	d.Teams[team] += amount

	// Save to file data.json
	err := d.Save()
	if err != nil {
		return
	}
}

func (d *Data) AddStreamer(streamer string, amount int64) {
	d.Lock()
	defer d.Unlock()

	d.Streamers[streamer] += amount

	// Save to file data.json
	err := d.Save()
	if err != nil {
		return
	}
}

func (d *Data) AddTotal(amount int64) {
	d.Lock()
	defer d.Unlock()

	d.Total += amount
}

func (d *Data) GetTeam(team string) int64 {
	d.Lock()
	defer d.Unlock()

	return d.Teams[team]
}

func (d *Data) GetStreamer(streamer string) int64 {
	d.Lock()
	defer d.Unlock()

	return d.Streamers[streamer]
}

func (d *Data) GetTotal() int64 {
	d.Lock()
	defer d.Unlock()

	return d.Total
}

func (d *Data) GetTeamFromStreamer(streamer string) string {
	d.Lock()
	defer d.Unlock()

	return d.StreamersToTeams[streamer]
}

func (d *Data) SetTeamForStreamer(streamer, team string) {
	d.Lock()
	defer d.Unlock()

	d.StreamersToTeams[streamer] = team

	// Save to file data.json
	err := d.Save()
	if err != nil {
		return
	}
}

func (d *Data) AddTombola(amount int64) {
	d.Lock()
	defer d.Unlock()

	d.Tombola += amount

	// Save to file data.json
	err := d.Save()
	if err != nil {
		return
	}
}

func (d *Data) GetTombola() int64 {
	d.Lock()
	defer d.Unlock()

	return d.Tombola
}

func (d *Data) AddToDonateur(donateur Donateur) {
	d.Lock()
	defer d.Unlock()

	id := donateur.ID
	team := donateur.Team

	// Update team donateurs
	temp, ok := d.Donateurs[team]
	if !ok {
		temp = make(map[string]Donateur)
		temp[id] = donateur
	}

	tempDonateurTeam, ok := temp[id]
	if !ok {
		temp[id] = donateur
		tempDonateurTeam = donateur
	} else {
		tempDonateurTeam.Amount += donateur.Amount
		tempDonateurTeam.Time = donateur.Time
	}

	d.Donateurs[team][id] = tempDonateurTeam

	// Update global donateurs
	temp, ok = d.Donateurs["global"]
	if !ok {
		temp = make(map[string]Donateur)
		temp[id] = donateur
	}

	tempDonateurGlobal, ok := temp[id]
	if !ok {
		temp[id] = donateur
		tempDonateurGlobal = donateur
	} else {
		tempDonateurGlobal.Amount += donateur.Amount
		tempDonateurGlobal.Time = donateur.Time
	}

	d.Donateurs["global"][id] = tempDonateurGlobal

	// Update last donateurs
	var found bool
	for i, lastDonateur := range d.LastDonateurs {
		if lastDonateur.ID == donateur.ID {
			d.LastDonateurs[i].Amount += donateur.Amount
			d.LastDonateurs[i].Time = donateur.Time
			found = true
			break
		}
	}
	if !found {
		d.LastDonateurs = append(d.LastDonateurs, donateur)
	}
	if len(d.LastDonateurs) > 10 {
		d.LastDonateurs = d.LastDonateurs[1:]
	}

	// d.InsertDonateurTeamTop(tempDonateurTeam)
	// d.InsertDonateurGlobalTop(tempDonateurGlobal)

	// Save to file data.json
	err := d.Save()
	if err != nil {
		return
	}
}

// func (d *Data) InsertDonateurTeamTop(donateur Donateur) {
// 	team := donateur.Team

// 	// Insertion sort on top for team and global
// 	teamTops := d.TeamTops[team]
// 	if len(teamTops) == 0 {
// 		teamTops = append(d.TeamTops[team], donateur)
// 	} else {
// 		// If he's already in the top, we replace and sort using sort.SliceStable
// 		var found bool
// 		for i, top := range teamTops {
// 			if top.ID == donateur.ID {
// 				teamTops[i] = donateur
// 				found = true
// 				break
// 			}
// 		}

// 		if !found {
// 			// Try to insert, if it's not in the top 10, we don't care
// 			teamTops = append(d.TeamTops[team], donateur)
// 		}

// 		sort.SliceStable(teamTops, func(i, j int) bool {
// 			return teamTops[i].Amount > teamTops[j].Amount
// 		})

// 		if len(teamTops) > 10 {
// 			teamTops = teamTops[:10]
// 		}
// 	}

// 	d.TeamTops[team] = teamTops
// }

// func (d *Data) InsertDonateurGlobalTop(donateur Donateur) {
// 	// Same for global
// 	globalTops := d.GlobalTop
// 	if len(globalTops) == 0 {
// 		globalTops = append(d.GlobalTop, donateur)
// 	} else {
// 		// If he's already in the top, we replace and sort using sort.SliceStable
// 		var found bool
// 		for i, top := range globalTops {
// 			if top.ID == donateur.ID {
// 				globalTops[i] = donateur
// 				found = true
// 				break
// 			}
// 		}

// 		if !found {
// 			// Try to insert, if it's not in the top 10, we don't care
// 			globalTops = append(d.GlobalTop, donateur)
// 		}

// 		sort.SliceStable(globalTops, func(i, j int) bool {
// 			return globalTops[i].Amount > globalTops[j].Amount
// 		})

// 		if len(globalTops) > 10 {
// 			globalTops = globalTops[:10]
// 		}
// 	}

// 	d.GlobalTop = globalTops
// }
