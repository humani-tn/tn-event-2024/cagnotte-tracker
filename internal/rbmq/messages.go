package rbmq

import (
	"fmt"
	"poc-helloasso/internal/cagnottes"
	"sort"
)

func SendStreamerPotInfo(streamerID string, pot int64) {
	topic := fmt.Sprintf("streamers.%s.pot", streamerID)
	msg := map[string]interface{}{
		"d": map[string]interface{}{
			"streamerID": streamerID,
			"pot":        pot,
		},
		"r": topic,
	}

	SendMessage(msg, topic)
}

func SendTeamPotInfo(teamID string, pot int64) {
	topic := fmt.Sprintf("teams.%s.pot", teamID)
	msg := map[string]interface{}{
		"d": map[string]interface{}{
			"teamID": teamID,
			"pot":    pot,
		},
		"r": topic,
	}

	SendMessage(msg, topic)
}

func SendTotalPotInfo(pot int64) {
	topic := "pot"
	msg := map[string]interface{}{
		"d": map[string]interface{}{
			"pot": pot,
		},
		"r": topic,
	}

	SendMessage(msg, topic)
}

func SendTombolaInfo(amount int64) {
	topic := "tombola"
	msg := map[string]interface{}{
		"d": map[string]interface{}{
			"pot": amount,
		},
		"r": topic,
	}

	SendMessage(msg, topic)
}

func SendDonationTeamInfo(teamID string, donateur cagnottes.Donateur) {
	topic := fmt.Sprintf("teams.%s.don", teamID)
	msg := map[string]interface{}{
		"d": map[string]interface{}{
			"teamID": teamID,
			"don":    donateur,
		},
		"r": topic,
	}

	SendMessage(msg, topic)
}

func SendTeamTopDonateurInfo(teamID string) {
	topic := fmt.Sprintf("tops.teams.%s", teamID)

	top := make([]cagnottes.Donateur, 0)
	for _, donateur := range cagnottes.D.Donateurs[teamID] {
		top = append(top, donateur)
	}
	sort.SliceStable(top, func(i, j int) bool {
		if top[i].Amount == top[j].Amount {
			return top[i].ID < top[j].ID
		}
		return top[i].Amount > top[j].Amount
	})
	if len(top) > 10 {
		top = top[:10]
	}

	msg := map[string]interface{}{
		"d": map[string]interface{}{
			"teamID": teamID,
			"dons":   top,
		},
		"r": topic,
	}

	SendMessage(msg, topic)
}

func SendGlobalTopDonateurInfo() {
	topic := "tops.global"

	top := make([]cagnottes.Donateur, 0)
	for _, donateur := range cagnottes.D.Donateurs["global"] {
		top = append(top, donateur)
	}
	// sort by id then by amount
	sort.SliceStable(top, func(i, j int) bool {
		if top[i].Amount == top[j].Amount {
			return top[i].ID < top[j].ID
		}
		return top[i].Amount > top[j].Amount
	})
	if len(top) > 10 {
		top = top[:10]
	}

	msg := map[string]interface{}{
		"d": map[string]interface{}{
			"dons": top,
		},
		"r": topic,
	}

	SendMessage(msg, topic)
}

func SendLastDonateursInfo() {
	topic := "tops.lasts"

	msg := map[string]interface{}{
		"d": map[string]interface{}{
			"dons": cagnottes.D.LastDonateurs,
		},
		"r": topic,
	}

	SendMessage(msg, topic)
}
