package rbmq

import (
	"context"
	"encoding/json"
	"poc-helloasso/internal/config"

	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/sirupsen/logrus"
)

// SendMessage sends a message to the exchange
func SendMessage(message map[string]interface{}, key string) error {
	body, err := json.Marshal(message)
	if err != nil {
		return err
	}

	cfg := config.GetConfig()

	logrus.Debug("Sending message to queue: ", string(body))

	for channel == nil {
		// reconnect
		connection, channel = connect()
	}

	return channel.PublishWithContext(
		context.Background(),
		cfg.Rbmq.ExchangeName,
		key,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(body),
		},
	)
}
