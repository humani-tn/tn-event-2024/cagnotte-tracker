package config

import (
	"fmt"

	"github.com/caarlos0/env/v9"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
)

type Config struct {
	HelloAssoConfig struct {
		URL          string `env:"URL" envDefault:"https://api.helloasso.com"`
		ClientID     string `env:"CLIENT_ID"`
		ClientSecret string `env:"CLIENT_SECRET"`
		Slug         string `env:"SLUG"`
		TombolaSlug  string `env:"TOMBOLA_SLUG"`
		ShopSlug     string `env:"SHOP_SLUG"`
	} `envPrefix:"HELLOASSO_"`

	TnEventConfig struct {
		URL string `env:"URL" envDefault:"https://dev.tnevent.telecomnancy.net/api"`
	} `envPrefix:"TNEVENT_"`

	Rbmq struct {
		ConnectionString string `env:"CONNECTION_STRING"`
		ExchangeName     string `env:"EXCHANGE_NAME"`
	} `envPrefix:"RBMQ_"`

	DiscordWebhook string `env:"DISCORD_WEBHOOK"`
	Port           string `env:"PORT" envDefault:"8080"`
	BaseURL        string `env:"BASE_URL" envDefault:"http://localhost:8080"`
	LogLevel       string `env:"LOG_LEVEL" envDefault:"info"`
}

var config Config

func GetConfig() Config {
	return config
}

func init() {
	godotenv.Load()
	if err := env.Parse(&config); err != nil {
		logrus.Fatal(err)
	}

	logrus.SetLevel(logrus.InfoLevel)
	if config.LogLevel == "debug" {
		logrus.SetLevel(logrus.DebugLevel)
	}

	logrus.Info("Loaded config: ", fmt.Sprintf("%+v", config))
}
