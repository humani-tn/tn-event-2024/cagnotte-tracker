package webhook

import (
	"bytes"
	"fmt"
	"net/http"
	"poc-helloasso/internal/config"
)

func SendMessageToDiscord(message string) error {
	conf := config.GetConfig()

	body := []byte(fmt.Sprintf(`{"content": "%s"}`, message))
	reader := bytes.NewReader(body)
	resp, err := http.Post(conf.DiscordWebhook, "application/json", reader)
	if err != nil {
		return err
	}
	if resp.StatusCode != 204 {
		return fmt.Errorf("discord webhook returned %d", resp.StatusCode)
	}

	return nil
}
